#include "config.h"
#include "WiFiNet.h"
#include "Sprocket.h"
//#include "OtaTcpPlugin.h"

WiFiNet wifi(
    SPROCKET_MODE,
    STATION_SSID,
    STATION_PASSWORD,
    AP_SSID,
    AP_PASSWORD,
    HOSTNAME,
    CONNECT_TIMEOUT);
    
Sprocket sprocket(
    {STARTUP_DELAY, SERIAL_BAUD_RATE});

void setup()
{
    delay(3000);
    wifi.connect();
    //sprocket.addPlugin(new OtaTcpPlugin({8266, ""}));
    sprocket.activate();
}

void loop()
{
    sprocket.loop();
    yield();
}