#ifndef __STANDALONE_CONFIG__
#define __STANDALONE_CONFIG__

// Scheduler config
#define _TASK_PRIORITY // Support for layered scheduling priority
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION

// Chip config
#define SERIAL_BAUD_RATE    115200
#define STARTUP_DELAY       3000

// network config
#define SPROCKET_MODE        0
#define AP_SSID             "MyAP"
#define AP_PASSWORD         "myApPwd"
#define STATION_SSID        "Th1ngs4p"
#define STATION_PASSWORD    "th3r31sn0sp00n"
#define HOSTNAME            "standalone-node"
#define CONNECT_TIMEOUT     10000

// OTA config
#define OTA_PORT 8266
#define OTA_PASSWORD ""

#endif