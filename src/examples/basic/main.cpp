#include "config.h"
#include "WiFiNet.h"
#include "Sprocket.h"

using namespace std;
using namespace std::placeholders;

WiFiNet *wifi;
Sprocket *sprocket;

void setup()
{
    wifi = new WiFiNet(
    SPROCKET_MODE,
    STATION_SSID,
    STATION_PASSWORD,
    AP_SSID,
    AP_PASSWORD,
    HOSTNAME,
    CONNECT_TIMEOUT);
    
    sprocket = new Sprocket(
        {STARTUP_DELAY, SERIAL_BAUD_RATE});
    wifi->connect();
    sprocket->activate();
    
}

void loop()
{
    sprocket->loop();
    yield();
}
 
