#ifndef __WIFI_NET__
#define __WIFI_NET__ 



#ifdef ESP32
#include <WiFi.h>
#include <ESPmDNS.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#endif // ESP32

#include "Network.h"

#include <ArduinoJson.h>
#include <FS.h>
#include "JsonStruct.h"
using namespace std;
using namespace std::placeholders;

#define JSON_stationMode "stationMode"
#define JSON_stationSSID "stationSSID"
#define JSON_apPassword "apPassword"
#define JSON_apSSID "apSSID"
#define JSON_stationPassword "stationPassword"
#define JSON_hostname "hostname"
#define JSON_connect_timeout "connectTimeout"

struct WiFiConfig : public JsonStruct {
    int stationMode;
    String stationSSID;
    String stationPassword;
    String apSSID;
    String apPassword;
    String hostname;
    int connectTimeout;

     void mapJsonObject(JsonObject& root) {
        root[JSON_stationMode] = stationMode;
        root[JSON_stationSSID] = stationSSID;
        root[JSON_stationPassword] = stationPassword;
        root[JSON_apSSID] = apSSID;
        root[JSON_apPassword] = apPassword;
        root[JSON_hostname] = hostname;
        root[JSON_connect_timeout] = connectTimeout;
    }
    
    // Map a json object to this struct.
    void fromJsonObject(JsonObject& json) {
        stationMode = getIntAttrFromJson(json, JSON_stationMode, stationMode);
        stationSSID = getAttrFromJson(json, JSON_stationSSID, stationSSID);
        stationPassword = getAttrFromJson(json, JSON_stationPassword, stationPassword);
        apSSID = getAttrFromJson(json, JSON_apSSID, apSSID);
        apPassword = getAttrFromJson(json, JSON_apPassword, apPassword);
        hostname = getAttrFromJson(json, JSON_hostname, hostname);
        connectTimeout = getIntAttrFromJson(json, JSON_connect_timeout, connectTimeout);
    };
};

class WiFiNet : public Network {
    public:
        WiFiConfig config;
        WiFiNet(int stationMode, const char* stationSSID, const char* stationPassword, const char* apSSID, const char* apPassword, const char* hostname, int connectTimeout);
        int connect();
        int connectStation();
        int createAccessPoint();
        int startDNS();
        void configure(WiFiConfig);
        int isConnected(){
            return WiFi.status() == WL_CONNECTED;
        }
};

#endif